CLI (Command Line Interface) Commands

ls - list the files and folders contained by the current directory

pwd - present working directory (shows the current folder we are working)

cd <folder name/path to folder> - change the current flder directory

mkdir <folder name> - make directory/folder

touch <filename> - to create files

sublime text 
   >> a lightweight text/file editor. much more lightweight than Visual Studio Code. uses less memory which is important because we also have google chrome to open.

Configure our Git:

git config --global user.email "emailFromGitlab/GitHub"> 
	>>This allows us to identifu the account from gitlab or github who will push/upload files into out online Gitlab or Github services.

git config --global user.name "usernameFromGitlab/Github">
	>>This will allow us to identifu the name of the user who is trying to upload / push filesinto our online repository

Pushing for the very first time:
git init - allows us to initialize a local folder as a local git repo. This means is now tracked by git.

git add . - allows us to add all files and updates into a new commit/version of our files and folders to be uploaded to our online repo

git commit -m "<commitMessage>" - allows us to create a new version of our files and folders based on the updates added using git add.
	>>commit messages starts with a verb <added, discussions, updated index.html, change solution>
	>> at least less than 50 characters

	git remote add origin <gitUrlOnlineRepo> - this allows us to connect an online repo to an local repo. "origin" is the defailt or conventional name for an online repo.

	git push origin master - allows us to push or upload the latest commit created into our online repo that is designated as "origin". master is the branch to upload/push our commit to. master branchis the defaut version of our files in the repo. 

	pushing updates:

	git add . >> git commmit -m "<commit message>" >> git push origin master